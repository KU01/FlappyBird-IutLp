﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]

public class flappyMove : MonoBehaviour {

	public float force=3; // force du saut
	private bool mort = false; // statut de vie du personnage
	public Rigidbody2D rb;
	//public bool characterInQuicksand;
	public AudioClip sonSaut;	// le sonore du Jump 
	public AudioClip sonImpact;
	AudioSource audioSource;

	// Use this for initialization
	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
		audioSource = GetComponent<AudioSource>();

	}

	void FixedUpdate()
	{
		// click gauche de la souris ou appuie de la touche espace
		if ((Input.GetMouseButtonDown (0) || Input.GetKeyDown (KeyCode.Space)) && !mort) {
			
			audioSource.PlayOneShot(sonSaut, 0.7F);	// ajout d'un sonore pour le saut

			Debug.Log("Saut!!");
			rb.velocity = new Vector2 (0f, force);	// ajout de la force pour sauter

			// rotation du pesonnage
			transform.rotation = Quaternion.RotateTowards (Quaternion.Euler(0f,0f,0f), Quaternion.Euler(0f,0f,90f),rb.velocity.y * .9f);
		}
		else if(rb.velocity.y < -.05){
			// rotation inverse du pesonnage quand il redescend
			transform.rotation = Quaternion.RotateTowards (Quaternion.Euler(0f,0f,0f), Quaternion.Euler(0f,0f,270f),-rb.velocity.y);
		}
	}

	void OnCollisionEnter2D(Collision2D other){
		audioSource.PlayOneShot(sonImpact, 0.7F);	// ajout d'un sonore lors d'un impact avec un Obet
		// quand flappy touche un objet, fin de la partie
		Perdu ();
	}

	void Perdu(){
		mort = true; // sattut = perdu et fin du jeu
		Jeu.perdu = true;
	}

	void OnTriggerEnter2D(Collider2D other) {
		// quand on passe entre 2 ou 1 obstacle

		//characterInQuicksand = true;
		// incrémentation du score
		Jeu.score ++;
	}

	// Update is called once per frame
	void Update () {
		
	}
}
