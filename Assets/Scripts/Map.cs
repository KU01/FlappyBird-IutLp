﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour {

	public float espacement;
	public int seuil;
	public int obstaclesDepart;
	public float vitesse;
	public int nbSol;
	public GameObject ojbObstacle;
	public GameObject ojbObstacle2;
	//public GameObject ojbObstacle3;
	//public GameObject ojbObstacle4;
	//public GameObject ojbObstacle5;
	public GameObject objSol;
	public Jeu manager;

	public Rigidbody2D rb;

	private List<GameObject> obstacles = new List<GameObject>();
	private List<GameObject> obstacles2 = new List<GameObject>();
	//private List<GameObject> obstacles3 = new List<GameObject>();
	//private List<GameObject> obstacles4 = new List<GameObject>();
	//private List<GameObject> obstacles5 = new List<GameObject>();

	private List<GameObject> ground = new List<GameObject>();

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();

		/* créer le nombre obstacle */
		for (int i = 0; i < obstaclesDepart; i++) {
			// faire apparaitre un objet
			GameObject tmp = (GameObject)Instantiate(ojbObstacle, new Vector2((float)i * espacement + 4f,(float)Random.Range(-1,-1)),  Quaternion.identity);
			tmp.transform.parent = this.transform;
			obstacles.Add (tmp); // ajout mon objet temporaire
		}

		for (int i = 0; i < obstaclesDepart; i++) {
			// faire apparaitre un objet
			GameObject tmp = (GameObject)Instantiate(ojbObstacle2, new Vector2((float)i * espacement + 22f,(float)Random.Range(1,-1)),  Quaternion.identity);
			tmp.transform.parent = this.transform;
			obstacles2.Add (tmp); // ajout mon objet temporaire
		}
		/*
		for (int i = 0; i < obstaclesDepart; i++) {
			// faire apparaitre un objet
			GameObject tmp = (GameObject)Instantiate(ojbObstacle3, new Vector2((float)i * espacement + 15f,(float)Random.Range(5,3)),  Quaternion.identity);
			tmp.transform.parent = this.transform;
			obstacles3.Add (tmp); // ajout mon objet temporaire
		}

		for (int i = 0; i < obstaclesDepart; i++) {
			// faire apparaitre un objet
			GameObject tmp = (GameObject)Instantiate(ojbObstacle4, new Vector2((float)i * espacement + 20f,(float)Random.Range(-2,-1)),  Quaternion.identity);
			tmp.transform.parent = this.transform;
			obstacles4.Add (tmp); // ajout mon objet temporaire
		}

		for (int i = 0; i < obstaclesDepart; i++) {
			// faire apparaitre un objet
			GameObject tmp = (GameObject)Instantiate(ojbObstacle5, new Vector2((float)i * espacement + 25f,(float)Random.Range(-1,-1)),  Quaternion.identity);
			tmp.transform.parent = this.transform;
			obstacles5.Add (tmp); // ajout mon objet temporaire
		}
		*/
		// création de nbSol sol
		for (int i = 0; i < nbSol; i++) {
			GameObject tmp = (GameObject)Instantiate(objSol, new Vector2((float)(i* 25f + espacement) - 50f, -4f),  Quaternion.identity);
			tmp.transform.parent = this.transform;
			ground.Add (tmp); 
		}

	}
	
	// Update is called once per frame
	void Update () {
		// déplacement vertical
		rb.velocity = new Vector2 (-vitesse, 0f);

		// génère d'autres obstables si on dépasse le seuil
		if (Jeu.score >= (obstacles.Count - seuil)) {
			GenererObj ();
		}

		if (Jeu.score >= (obstacles2.Count - seuil)) {
			GenererObj ();
		}
		/*
		if (Jeu.score >= (obstacles3.Count - seuil)) {
			GenererObj ();
		}
		*/
		if (Jeu.score >= (ground.Count - 10)) {
			GenererSol ();
		}
	}
	/*
	* création d'un décor infini
	*/
	void GenererObj (){
		GameObject tmp = (GameObject)Instantiate(ojbObstacle, new Vector2(obstacles[obstacles.Count - 1].transform.position.x + espacement,(float)Random.Range(-1,-1)), Quaternion.identity);
		tmp.transform.parent = this.transform;
		obstacles.Add(tmp);

		GameObject tmp2 = (GameObject)Instantiate(ojbObstacle2, new Vector2(obstacles2[obstacles2.Count - 1].transform.position.x + espacement,(float)Random.Range(1,-1)), Quaternion.identity);
		tmp2.transform.parent = this.transform;
		obstacles2.Add(tmp2);
		/*
		GameObject tmp3 = (GameObject)Instantiate(ojbObstacle3, new Vector2(obstacles3[obstacles3.Count - 1].transform.position.x + espacement,(float)Random.Range(5,3)), Quaternion.identity);
		tmp3.transform.parent = this.transform;
		obstacles3.Add(tmp3);
		*/

	}

	/*
	 * Prolonge le sol si on arrive au bout
	 */
	void GenererSol (){
		GameObject tmp = (GameObject)Instantiate(objSol, new Vector2(ground[ground.Count - 1].transform.position.x + espacement,(float)Random.Range(-50f,-4f)), Quaternion.identity);
		tmp.transform.parent = this.transform;
		ground.Add(tmp);
	}

}
