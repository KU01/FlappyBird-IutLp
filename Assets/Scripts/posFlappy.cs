﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class posFlappy : MonoBehaviour {

	private Vector3 leftBottomCameraBorder;
	private Vector3 rightBottomCameraBorder;
	private Vector3 leftTopCameraBorder;
	private Vector3 rightTopCameraBorder;
	private Vector3 siz;

	// Use this for initialization
	void Start()
	{
		// création de 4 murs
		leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
		rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
		leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
		rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

	}

	// Update is called once per frame
	void Update()
	{

		// calcul de la taille du sprite
		// on limit la taille de l'écran à ne pas dépasser 

		siz.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;//= gameObject.transform.localScale.x;
		siz.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;


		gameObject.transform.position = new Vector3 (leftTopCameraBorder.x + (siz.x / 2), transform.position.y, transform.position.z);

		if (transform.position.y < leftBottomCameraBorder.y + (siz.y / 2))
			gameObject.transform.position = new Vector3 (transform.position.x, leftBottomCameraBorder.y + (siz.y / 2), transform.position.z);


		if (transform.position.y > leftTopCameraBorder.y - (siz.y / 2))
			gameObject.transform.position = new Vector3 (transform.position.x, leftTopCameraBorder.y - (siz.y / 2), transform.position.z);
	}		
}
