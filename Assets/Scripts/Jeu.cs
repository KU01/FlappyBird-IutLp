﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Jeu : MonoBehaviour {

	public static int score; // le résultat du score et en static pour être vu par les autres Class
	private int meilleurScore;
	public static bool perdu = false; // 2 statuts, gagné ou perdu
	//public GUIText texte;
	public Text texte;
	public Text best_score;

	// Use this for initialization
	void Start () {
		score = 0;	// initialisation du score
		meilleurScore = PlayerPrefs.GetInt ("meilleurscore"); // sauvegarde du score
		best_score.text = "BEST SCORE : " + meilleurScore; // affichage du meilleur score
	}
	
	// Update is called once per frame
	void Update () {
		// sauvegarde du nouveau meilleur score
		if (perdu && score > meilleurScore) {
			meilleurScore = score;
			PlayerPrefs.SetInt ("meilleurscore", score);
		} 
	}

	// affichage des scores
	public void OnGUI(){
		// affiche de score Nombre en Texte
		GUI.Label (new Rect (Screen.width/2 - 100, 15f, 200, 180), score.ToString());
		if (perdu) {
			// affiche un message de fin de jeu
			texte.text = "Game Over";

			/* 
			* le score, meilleur score et rejouer 
			*/
			// bouton à rejouer
			if (GUI.Button (new Rect (Screen.width / 2 - 100, Screen.height / 2 , 200, 50), "Rejouer ?")) {
				//Application.LoadLevel ("flappyBirdGame");
				SceneManager.LoadScene ("flappyBirdGame");
				meilleurScore = PlayerPrefs.GetInt ("meileurscore");

			}

			// bouton à quitter
			if (GUI.Button (new Rect (Screen.width / 2 - 100, Screen.height / 2 + 50, 200, 50), "Quitter")) {
				Application.Quit();
				Debug.Log("Fin du Jeu");

			}
		}
	}
}
