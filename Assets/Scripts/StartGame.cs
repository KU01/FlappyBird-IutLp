﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour {

	// Initialisations des variables
	public GUISkin style;
	private bool showmsg = true;


	// Use this for initialization
	void Start () {
		// Début du Jeu en pause
		Time.timeScale = 0.0f; // valeur en flottant
	
	}
	
	void OnGUI(){
		if (showmsg) {
			GUI.skin = style; // style du texte
			// position du bouton Start, Taille de l'écran à appuyer, taille du bouton (en px) + message 
			GUI.TextField (new Rect (Screen.width / 2 - 70, Screen.height / 2 - 15, 120, 30), "START");

			// si un bouton est touché
			if (Input.anyKeyDown) {
				// démarrage le jeu
				Time.timeScale = 1.0f;
				showmsg = false;
				// le bouton Start disparait
				GetComponent<SpriteRenderer>().gameObject.SetActive (false);
			}
		}
	}
}
