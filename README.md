Name of project 
============================================================
Projet Unity - LPro 2017-2018


Description
============================================================
Développer un jeu vidéo avec Unity :

_ sujet choisi, TD2 - développer le jeu Flappy Bird


Requirement
============================================================
[Unity 2017 minimum](https://unity3d.com/fr/get-unity/download) 

[MonoDevelop](http://www.monodevelop.com/download/)


Command line instructions
============================================================
## 1. Cloner le git
git clone https://gitlab.com/KU01/FlappyBird-IutLp.git


## 2. Ouvrir le projet avec Unity
Sélectionner un nouveau projet à ouvrir puis prendre le dossier :

FlappyBird-IutLp


## 3. Jouer
Cliquer sur Play de Unity pour jouer


## 4. Tester l'application sur le Web (Si l'application ne fonctionne pas, copier le dossier dans votre serveur Web local www/ )
Ouvrir le fichier suivant dans un navigateur :

flappyBird_web/index.html


## 5. A propos sur l'application
L'application fonctionne sous Unity et sur le Web


## 6. Support
Pour développer le jeu, je me suis aidé des tutos et des cours suivants :

_ formation-facile.fr par Anthony Cardinale

_ Creationjeuxjava

_ [Unity3d.com](https://unity3d.com/fr/learn/tutorials/topics/2d-game-creation/bird-script)


NOTE - REMARQUE
============================================================
## Note :

_ le personnage saute avec la touche "espace" ou le clic gauche de la souris.


## Les difficultés rencontrées :

_ j'ai très peu utilisé vos tutos pour développer le jeu.

_ l'affichage de la fin du jeu n'est pas opérationnel à 100%, le message "game over" et les boutons "rejouer" et "quitter" ne disparaissent pas lors d'une nouvelle partie ou à la fin de la partie.

_ le bouton "quitter" ne fonctionne pas.
